#ifndef _MANAGER_H
#define _MANAGER_H

#include <iostream>
#include <list>
#include <fstream>
#include <string>
#include <iomanip>
#include <sstream>
#include"Date.h"
#include"StringTokenizer.h"
#include"Assignment.h"
#include"List.h"
/*Globals*/

struct Manager
{
	// The lists
	List AssignedList;
	List CompletedList;

	//functions
	HW makeAssignment();
	void readIn(string filename);
	void saveDB(string filename);
	void showMenu();
};

HW Manager::makeAssignment()
{
	// get date info from user
	cin.ignore(2, '\n');
	string assigned, due, desc;
	int stat;
	cout << "Enter description:\n";
	getline(cin, desc);
	cout << "Enter assigned date: (ex. 2-23-2007): \n";
	getline(cin, assigned);
	cout << "Enter due date: \n";
	getline(cin, due);
	cout << "Enter status 0,1, or 2 (assigned, completed, late):\n";
	cin >> stat;
	// make sure dates are valid
	while (Date(assigned) > Date(due))
	{
		cout << "Invalid Dates!\n";
		cin.ignore(1, '\n');
		cout << "Enter assigned date: (ex. 2-23-2007): \n";
		getline(cin, assigned);
		cout << "Enter due date: \n";
		getline(cin, due);
	}
	// create Assignment object and return it with the given parameters
	HW toReturn;
	toReturn.setAll(Date(due), Date(assigned), desc, status(stat));
	return toReturn;
}
void Manager::readIn(string filename) // filename for input.txt, assigned list, completed list
{

	// create input file stream with filename
	ifstream fin(filename, ios::in);
	if (!fin.good())//if file is bad, exit
	{
		cout << "Invalid file!\n";
		return;
	}
	// temporary strings to hold line its 'token' data from input file
	string line, token;
	while (!fin.eof()) // while not end of file
	{
		HW toAdd; //create assignment object to add to List
		getline(fin, line); // read in entire line
		if (fin.bad())return;
		if (fin.fail()) return;

		// create string stream out of line to instantiate tokens
		istringstream stream(line);
		getline(stream, token, ',');//using the comma as the delimeter

									// some spaces exist after the commas, we do not want them in our string representations
		if (token.front() == ' ')
			token.erase(0, 1);
		toAdd.editDue(Date(token)); // first item in input file is due date, assign to object

									// repeat process for entire line
		getline(stream, token, ',');
		if (token.front() == ' ')
			token.erase(0, 1);
		toAdd.setDescription(token);// set description

		getline(stream, token, ',');
		if (token.front() == ' ')
			token.erase(0, 1);
		toAdd.setAssignedDate(token); //set date assigned

		getline(stream, token, ',');
		if (token.front() == ' ')
			token.erase(0, 1);
		// create int for enum status, use evalStatus() function to read in text from file and determine enum value
		int a = evalStatus(token);
		toAdd.editStatus(a); //assign the evaluated enum value to assignment object
							 // Now we determine which list the assignment belongs in based on its status
		if (a == 0) // 0 means assigned status
		{
			AssignedList.addAssignment(toAdd);
		}
		else if (a == 1) // means completed
		{
			CompletedList.addAssignment(toAdd);
		}
		else if (a == 2) // late, increment late assignment counter
		{
			CompletedList.addAssignment(toAdd);
		}
	}
	fin.close();
}
void Manager::saveDB(string filename)
{
	ofstream fout(filename, ios::out);
	// First the data in the Assigned list
	// create an iterator to parse the lists
	list<HW>::iterator iter = AssignedList.theList.begin();
	while (iter != AssignedList.theList.end())
	{
		// since this is the Assigned list we know the status is always "assigned"
		fout << iter->getDue().toString() << ", " << iter->getDescription() << ", " << iter->getAssigned().toString() << ", assigned";
		if (iter != AssignedList.theList.end()) fout << "\n";
		iter++;
	}
	// now repeat for the Completed list
	iter = CompletedList.theList.begin();
	while (iter != CompletedList.theList.end())
	{
		switch (iter->getStatus())
		{
		case 1: // 1 = completed
			fout << iter->getDue().toString() << ", " << iter->getDescription() << ", " << iter->getAssigned().toString() << ", completed"; //writes line to file
			if (iter != CompletedList.theList.end()) fout << "\n";
			iter++;
			break;
		case 2: // 2 = late
			fout << iter->getDue().toString() << ", " << iter->getDescription() << ", " << iter->getAssigned().toString() << ", late"; //writes line to file
			if (iter != CompletedList.theList.end()) fout << "\n";
			iter++;
			break;
		default:
			cout << "Status Error\n"; // for debugging purposes
			break;
		}
	}
	fout.close();

}
void Manager::showMenu()
{
	while (true)
	{
		int choice;
		cout << "(1) Read data in from file\n" <<
			"(2) Save data to file\n" <<
			"(3) Display 'Assigned' list (current)\n" <<
			"(4) Display 'Completed' list (past due)\n" <<
			"(5) Add Assignment\n" <<
			"(6) Grade Assignment\n" <<
			"(7) Edit Assignment Data\n" <<
			"(8) Display LATE Assignemnts\n(9) Sort\n(10) Number of Late assignments\n(0) Quit\n";
		cin >> choice;
		if ((choice < 0) || (choice > 10)) continue;
		switch (choice)
		{
		case 0:
		// exit program
		{
			cin.ignore(1, '\n');
			cout << "Save changes?( enter 'y')\n";
			char j;
			cin >> j;
			if (j == 'y' || j == 'Y')
			{
				cin.ignore(1, '\n');

				cout << "Enter the file name to save to\n";
				string filename;
				getline(cin, filename);
				saveDB(filename);
			}
			exit(0);
			break;
		}
		case 1:
		// read data in from txt file
		{
			cin.ignore(1, '\n');
			cout << "Enter the input filename:\n";
			string file;
			getline(cin, file);
			readIn(file);
			break;
		}
		case 2:
		// save data to txt file
		{
			cin.ignore(1, '\n');
			cout << "Enter the save filename:\n";
			string file;
			getline(cin, file);
			saveDB(file);
			break;
		}
		case 3:
		// display the Assigned List
			AssignedList.displayList(0);
			break;
		case 4:
		// display the Compelted List
			CompletedList.displayList(0);
			break;
		case 5:
		 // add assignment to proper list based on status
		{
			HW toAdd = makeAssignment();
			if (toAdd.getStatus() == status(DUE))
				if(!CompletedList.inList(toAdd.getAssigned()))
					AssignedList.addAssignment(toAdd);
			else if ((toAdd.getStatus() == status(LATE)) || (toAdd.getStatus() == status(DUE)))
				if(!AssignedList.inList(toAdd.getAssigned()))
					CompletedList.addAssignment(toAdd);
			break;
		}
		case 6:
		// grade assignment, assign proper status based on completion date
		{
			HW toAdd2 = AssignedList.grade();
			CompletedList.addAssignment(toAdd2);
			break;
		}
		case 7:
		// edit an Assignment in the Assigned List
		{
			cin.ignore(1, '\n');
			string date;
			cout << "Enter the assigned date of assignment you wish to edit (ex 2-25-2016):\n";
			getline(cin, date);
			AssignedList.editAssignment(date);
			break;
		}
		case 8:
		// display function with a 1 flag only shows LATE assignments
			CompletedList.displayList(1);
			break;
		case 9:
		// sorts list without display
			CompletedList.listSort();
			AssignedList.listSort();
		case 10:
		// shows number of late assignments
			cout << CompletedList.numLate << " LATE assignments\n";
			break;
		default:
			cout << "Error invalid menu selection!\n";
			break;
		}

	}

}
#endif