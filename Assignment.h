#ifndef _ASGN_H
#define _ASGN_H

enum status { DUE, COMPLETED, LATE };

// HomeWork (HW) is much easy to write than assignment.
class HW {
	Date dueDate;
	Date assignedDate;
	string description;
	status stat;
public:
	HW();
	//overload constructor to take args
	void setAll(Date due, Date asg, string des, status s);
	void editDue(Date newDue);  // works as edit
	void editDescription();
	void editStatus(int a); //0 - DUE, 1 - COMPLETED, 2 - LATE
	void showAll();
	void setDescription(string a);
	void setAssignedDate(string b);
	Date getDue();
	Date getAssigned();
	string getDescription();
	status getStatus();
};

HW::HW()
// default constructor for HW class
{
	Date dueDate(2016, 12, 31); //default values
	Date assignedDate(2016, 1, 1);//default values - arbitrary
	description = "NULL";
	status stat = status(DUE);
}
void HW::setAll(Date due, Date asg, string des, status s)
// function to set all attribute values
{
	dueDate = due;
	assignedDate = asg;
	description = des;
	stat = s;
}
void HW::editDescription() {
	while (true)
		// loop to give ability to change description until confirmed
	{
		cout << "Enter the description for the assignment\n";
		string desc;
		getline(cin, desc);
		cout << "\nThe new description is:\n" << desc
			<< "\nConfirm changes(y)? Revert changes (n)?\nEnter anything else to re-enter\n";
		char c;
		cin >> c;
		if (c == 'y' || c == 'Y')
		{
			// if user confirms changes, make actual change and break
			description = desc;
			break;
		}
		// keeps description the same
		else if (c == 'n' || c == 'N')
			break;
	}
}
void HW::editStatus(int a)
//change status of assignment, pass in 0, 1 or 2 : enum(due,completed,late)
{
	if (a == 0)
		stat = status(DUE);
	else if (a == 1)
		stat = status(COMPLETED);
	else if (a == 2)
		stat = status(LATE);
	else
		cout << "Invalid Entry\n";
}
void HW::editDue(Date newDue) {
	dueDate = newDue;
}
void HW::showAll()
{
	cout << "Assignment Description:\n" << description
		<< "\nAssigned: " << assignedDate.toString() << "\nDue: " << dueDate.toString()
		<< "\nStatus: " << status(stat) << "\n(0 - DUE, 1 - COMPLETED, 2 - LATE)\n" << endl;
}
Date HW::getDue()
{
	return dueDate;
}
Date HW::getAssigned()
{
	return assignedDate;
}
string HW::getDescription()
{
	return description;
}
status HW::getStatus()
{
	return stat;
}
void HW::setDescription(string a)
{
	description = a;
}
void HW::setAssignedDate(string b)
{
	assignedDate = Date(b);
}
#endif 