#ifndef _LIST_H_
#define _LIST_H_
// prototype functions for use in List functions//
bool dateCompare(HW& a, HW& b);// used in built in sort function for linked lists
int evalStatus(string STATUS); // takes a string status and returns its enum value;
string stringStatus(status a)  // similar to evalStatus, function takes the enum as an argument and returns the string version for file output
{
	switch (a)
	{
	case 0: //0 enum is assigned
		return " assigned";
		break;
	case 1:  //1 enum is completed
		return " completed";
		break;
	case 2: //2 enum is late
		return " late";
		break;
	default:
	{
		string error = "error: bad enum value\n";
		cout << error;
		return error;
		break;
	}
	}
}
bool dateCompare(HW& a, HW& b) //for use in list sort
{
	return a.getDue() > b.getDue();
}
int evalStatus(string STATUS) //returns proper enum value for given strings
{
	// remove as many beginning white spaces as needed until we are left with just the string
	while (STATUS.front() == ' ')
		STATUS.erase(0, 1);

	if (STATUS == "assigned")
		return 0;
	else if (STATUS == "completed")
		return 1;
	else if (STATUS == "late")
		return 2;
	else
		cout << "Error invalid status\n";
	return -1;
}

// list struct with necessary functions
struct List 
{
	// the actual linked list of HW objects ('Home Work') 
	list <HW> theList;
	//number of late items
	int numLate;
	/***********functions***********/
	List();	// constructor
	void listSort();
	void displayList(int a); // display all items in list, int a is a flag for show all (0) or only show late (1)
	HW grade();
	void addAssignment(HW assignment);
	void editAssignment(string date);
	bool inList(Date assignedDate);
};

void List::editAssignment(string date)
{
	list<HW>::iterator iter = theList.begin();
	while (iter != theList.end())
	{
		if (iter->getAssigned().toString() == date)
		{
			cout << "(1) Edit Due Date\n(2) Edit Description\n";
			int c;
			cin >> c;
			if (c == 1)
			{
				string newdate;
				cout << "Enter the new due date (ex. 3-20-2011):\n";
				getline(cin, newdate);
				iter->editDue(Date(newdate));
				return;
			}
			else if (c == 2)
			{
				iter->editDescription();
				return;
			}
			else cout << "Invalid Entry!\n";

		}
	}
	cout << "\nNo match found!\n";
}
void List::listSort() // takes a list and sorts it be due date
{
	theList.sort(dateCompare);
}

List::List()
{
	list <HW> theList;
	int numLate = 0;
}
void List::displayList(int a)
{
	//option to sort list before display
	cout << "Enter 'y' to sort list before display: ";
	char y;
	cin >> y;
	if (y == 'y')
	{
		listSort();
	}
	//create iterator to parse the list
	list <HW>::iterator iter = theList.begin();
	cout << left << setw(18) << "Description" << setw(15) << " Assigned " << setw(15) << " Due " << setw(6) << " Status\n" <<"-------------------------------------------------------------------\n";
	if (a == 1)
	{
		while (iter != theList.end())
		{
			if ((iter->getStatus() == status(LATE)))
			{
				cout<< left << setw(18) << iter->getDescription() << setw(15) <<iter->getAssigned().toString() << setw(15) << iter->getDue().toString() << setw(6)<< stringStatus(iter->getStatus()) << "\n";
			}
			iter++;
		}
	}
	else if(a==0)
		while (iter != theList.end())
		{
			{
				cout << left << setw(18) << iter->getDescription() << setw(15) << iter->getAssigned().toString() << setw(15) << iter->getDue().toString() << setw(6) << stringStatus(iter->getStatus()) << "\n";
			}
			iter++;
		}
	cout << "\n";

}
HW List::grade()
{
	// get date of completion
	cin.ignore(2, '\n');
	string completed;
	cout << "Enter Assignment Completion Date (ex. 8-15-2014):\n";
	getline(cin, completed);
	
	//get assignment date
	string assigned;
	cout << "Enter Date Assigned:\n";
	getline(cin, assigned);
	Date COMPLETED(completed);
	Date ASSIGNED(assigned);
	//create iterator to check if matching assignment exists
	list <HW>::iterator iter = theList.begin();
	for (iter; iter != theList.end();iter++)
	{
		if (iter->getAssigned() == ASSIGNED)
		{
			// if date completed is before or on due date, mark assignment 'completed' and remove from current list.
			if (COMPLETED <= iter->getDue())
			{
				iter->editStatus(1); // 1 enumerates to a 'completed' status
				// make copy of updated assignment (status changed)
				HW copy = *iter;
				// remove original Assignment from list
				theList.erase(iter);
				// return the copy to be added to 'completed' list
				return copy;
			}
			else
			{
				iter->editStatus(2); //2 enumerates to a 'late'
				// make copy of updated assignment
				HW copy = *iter;
				numLate++; //increment number of late assignments counter
				theList.erase(iter);
				return copy;
			}
		}
	}
}
void List::addAssignment(HW assignment)
{
	// first check to make sure no assignment with same info exists (1 assignment per day)
	list<HW>::iterator iter = theList.begin();
	for (iter; iter != theList.end(); iter++) //loop through list
	{
		if (assignment.getAssigned() == iter->getAssigned()) //if assigned dates match, display error
		{
			cout << "Error: Assignment with same date already exists!\n";
			return;
		}
	}
	// no match found, add assignment to list
	if (assignment.getStatus() == status(LATE))numLate++; //increment late assignment counter if late
	theList.push_back(assignment);
}
bool List::inList(Date assignedDate)
{
	// if assignment with given date already exists in list, returns true
	list<HW>::iterator iter = theList.begin();
	while (iter != theList.end())
	{
		if (iter->getAssigned() == Date(assignedDate))
			return true;
		else
			iter++;
	}
	return false;
}
#endif 
